package com.bootcamp.bookifyAlpha.controllers;

import com.bootcamp.bookifyAlpha.models.BookifyApp;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

public class BookifyControllerTest {
    @Test
    public void should_return_bookify_instance()
    {
        BookifyController bookifyController = new BookifyController();
        assertEquals(new BookifyApp("Bookify-Alpha-Ajit-Guru").getName(), bookifyController.getTitle().getName());
    }
}
