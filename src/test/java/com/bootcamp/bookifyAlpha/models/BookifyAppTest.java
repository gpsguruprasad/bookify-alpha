package com.bootcamp.bookifyAlpha.models;

import com.bootcamp.bookifyAlpha.models.BookifyApp;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BookifyAppTest {
    @Test
    public void should_return_app_title(){
        BookifyApp bookifyApp = new BookifyApp("Test App");
        assertEquals("Test App", bookifyApp.getName());
    }
}
