package com.bootcamp.bookifyAlpha.models;

public class BookifyApp {
    private final String name;

    public BookifyApp(String title){
        this.name = title;
    }

    public String getName() {
        return name;
    }
}
