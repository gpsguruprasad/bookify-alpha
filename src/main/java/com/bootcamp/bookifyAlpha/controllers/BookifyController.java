package com.bootcamp.bookifyAlpha.controllers;

import com.bootcamp.bookifyAlpha.models.BookifyApp;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BookifyController {

    @GetMapping("/")
    BookifyApp getTitle() {
        return new BookifyApp("Bookify-Alpha-Ajit-Guru");
    }
}
