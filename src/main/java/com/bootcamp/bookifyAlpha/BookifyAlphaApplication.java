package com.bootcamp.bookifyAlpha;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BookifyAlphaApplication {

	public static void main(String[] args) {
		SpringApplication.run(BookifyAlphaApplication.class, args);
	}

}
